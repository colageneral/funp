function varargout = Loadwb(varargin)
% LOADWB MATLAB code for Loadwb.fig
%      LOADWB, by itself, creates a new LOADWB or raises the existing
%      singleton*.
%
%      H = LOADWB returns the handle to a new LOADWB or the handle to
%      the existing singleton*.
%
%      LOADWB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOADWB.M with the given input arguments.
%
%      LOADWB('Property','Value',...) creates a new LOADWB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Loadwb_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Loadwb_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Loadwb

% Last Modified by GUIDE v2.5 11-May-2018 16:29:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Loadwb_OpeningFcn, ...
                   'gui_OutputFcn',  @Loadwb_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Loadwb is made visible.
function Loadwb_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Loadwb (see VARARGIN)

% Choose default command line output for Loadwb
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Loadwb wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Loadwb_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on button press in Open.
function Open_Callback(hObject, eventdata, handles)
% hObject    handle to Open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
folder = uigetdir('./');
handles.ListName = folder;
guidata(hObject, handles);


% --- Executes on selection change in OpenListbox.
function OpenListbox_Callback(hObject, eventdata, handles)
% hObject    handle to OpenListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns OpenListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from OpenListbox
OpenList = handles.ListName;
set(handles.OpenListbox,'string',OpenList)
handles.OpenList = OpenList;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function OpenListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to OpenListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Add.
function Add_Callback(hObject, eventdata, handles)
% hObject    handle to Add (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
new = get(handles.OpenListbox, 'string');
old = get(handles.TotalListbox, 'string');
if ischar(old) == 1
    add = strvcat(old, new);
    set(handles.TotalListbox, 'String', add);
else
    set(handles.TotalListbox, 'String', new);
end
existingItems = cellstr(get(handles.TotalListbox,'String'));

if(isempty(existingItems))
else
    handles.Remove.Enable='on';
end

handles.total_list = add;
guidata(hObject, handles);


% --- Executes on button press in Remove.
function Remove_Callback(hObject, eventdata, handles)
% hObject    handle to Remove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
existingItems = cellstr(get(handles.TotalListbox,'String'));
selectedId = get(handles.TotalListbox,'Value');

if(class(existingItems) == 'char')
   upd_list='';
   set(handles.TotalListbox, 'String', upd_list)
else
   % If the returned list is a cell array there are three cases
   n_items=length(existingItems);
   if(selectedId == 1)
      % The first element has been selected
      upd_list={existingItems{2:end}};
   elseif(selectedId == n_items)
      % The last element has been selected
      upd_list={existingItems{1:end-1}};
      % Set the "Value" property to the previous element
      set(handles.TotalListbox, 'Value', selectedId-1)
   else
      % And element in the list has been selected
      upd_list={existingItems{1:selectedId-1} existingItems{selectedId+1:end}};
   end
end
% Update the list
set(handles.TotalListbox, 'String', upd_list)

if(isempty(existingItems))
   handles.Remove.Enable='off';
end


% --- Executes on selection change in TotalListbox.
function TotalListbox_Callback(hObject, eventdata, handles)
% hObject    handle to TotalListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns TotalListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from TotalListbox


% --- Executes during object creation, after setting all properties.
function TotalListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TotalListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fn = fieldnames(handles);
test = 0;
for i = 1:size(fn,1)
    if strcmp(fn{i}, 'total_list') == 1
        test = test + 1;
    end
end

if test == 1
    assignin('base', 'wb_folder', handles.total_list);
else
    error('### You did not select any data ###')
end
close(Loadwb)


% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(Loadwb)
