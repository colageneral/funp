function save_surface_T1(save_folder, T1_total_list, standard_path, standard_name, standard_ext, standard_mm)
    save(strcat(save_folder,'/FuNP_Surface_T1_settings.mat'), 'T1_total_list', 'standard_path', 'standard_name', 'standard_ext', 'standard_mm');
end