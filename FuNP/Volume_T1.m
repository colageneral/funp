function varargout = Volume_T1(varargin)
% VOLUME_T1 MATLAB code for Volume_T1.fig
%      VOLUME_T1, by itself, creates a new VOLUME_T1 or raises the existing
%      singleton*.
%
%      H = VOLUME_T1 returns the handle to a new VOLUME_T1 or the handle to
%      the existing singleton*.
%
%      VOLUME_T1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VOLUME_T1.M with the given input arguments.
%
%      VOLUME_T1('Property','Value',...) creates a new VOLUME_T1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Volume_T1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Volume_T1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Volume_T1

% Last Modified by GUIDE v2.5 04-Jul-2018 09:26:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Volume_T1_OpeningFcn, ...
                   'gui_OutputFcn',  @Volume_T1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Volume_T1 is made visible.
function Volume_T1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Volume_T1 (see VARARGIN)

% Choose default command line output for Volume_T1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Volume_T1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Volume_T1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




% --- Executes on button press in load_T1.
function load_T1_Callback(hObject, eventdata, handles)
% hObject    handle to load_T1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_LoadT1 = LoadT1;
handles.h_LoadT1 = guihandles(h_LoadT1);
guidata(hObject, handles);



% --- Executes on button press in Reorient.
function Reorient_Callback(hObject, eventdata, handles)
% hObject    handle to Reorient (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Reorient = Reorient;
handles.h_Reorient = guihandles(h_Reorient);
guidata(hObject, handles);


% --- Executes on button press in MFIcor.
function MFIcor_Callback(hObject, eventdata, handles)
% hObject    handle to MFIcor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_MFIcor = MFIcor;
handles.h_MFIcor = guihandles(h_MFIcor);
guidata(hObject, handles);



% --- Executes on button press in SkullRemoval.
function SkullRemoval_Callback(hObject, eventdata, handles)
% hObject    handle to SkullRemoval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_SkullRemoval_T1 = SkullRemoval_T1;
handles.h_SkullRemoval_T1 = guihandles(h_SkullRemoval_T1);
guidata(hObject, handles);



% --- Executes on button press in Registration.
function Registration_Callback(hObject, eventdata, handles)
% hObject    handle to Registration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Registration_T1 = Registration_T1;
handles.h_Registration_T1 = guihandles(h_Registration_T1);
guidata(hObject, handles);


% --- Executes on button press in Segmentation.
function Segmentation_Callback(hObject, eventdata, handles)
% hObject    handle to Segmentation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Segmentation = Segmentation;
handles.h_Segmentation = guihandles(h_Segmentation);
guidata(hObject, handles);


% --- Executes on button press in GO.
function GO_Callback(hObject, eventdata, handles)
% hObject    handle to GO (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
preproc_volume_T1(evalin('base','T1_total_list'), evalin('base','reorient'), evalin('base','orientation'), ...
    evalin('base','mficor'), evalin('base','skullremoval_T1'), evalin('base','regis_T1'), evalin('base','standard_path'), ...
    evalin('base','standard_name'), evalin('base','standard_ext'), evalin('base','dof'), evalin('base','segmentation'));
close(Volume_T1)

% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(Volume_T1)


% --- Executes on button press in SAVE.
function SAVE_Callback(hObject, eventdata, handles)
% hObject    handle to SAVE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
folder = uigetdir('./');
handles.ListName = folder;
guidata(hObject, handles);
save_folder = handles.ListName;

save_volume_T1(save_folder, evalin('base','T1_total_list'), evalin('base','reorient'), evalin('base','orientation'), ...
    evalin('base','mficor'), evalin('base','skullremoval_T1'), evalin('base','regis_T1'), evalin('base','standard_path'), ...
    evalin('base','standard_name'), evalin('base','standard_ext'), evalin('base','dof'), evalin('base','segmentation'));
