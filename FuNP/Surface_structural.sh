#!/bin/bash
set -e

Usage() {
    cat <<EOF
Usage: Surface_structural.sh <main folder> <subject ID> <freesurfer output forder> <surface atlas directory> <template folder> <templatename> <template mm>
  
  <main folder>			Path that output folders will be saved
  <subject ID>			Subject ID
  <freesurfer output folder>	Path that contain the outputs from the freesurfer recon-all
  <surface atlas directory>	Path that contain the surface atlas files
  <template folder>		Path that contain the template file
  <template name>		The name of the template
  <template mm>			The resolution (in mm) of the template

  example: Surface_structural.sh ~/Surface A00035561 ~/MPRAGE ~/Atlas/SurfaceAtlas ~/ MNI152_T1_3mm_brain 3mm
EOF
    exit 1
}

[ "$2" = "" ] && Usage

############################# Settings #############################
HeadFolder="$1"
Subject="$2"
FreeSurferFolder="$3"
SurfaceAtlasDIR="$4"
templateFolder="$5"
templateName="$6"
templatemm="$7"
####################################################################
HighResMesh="164"
LowResMesh="32"
HeadFolder="${HeadFolder}"/"${Subject}"
T1wFolder="${HeadFolder}/T1w"
AtlasSpaceFolder="${HeadFolder}/MNINonLinear"
FreeSurferLabels="$SurfaceAtlasDIR"/FreeSurferAllLut.txt
SubcorticalGrayLabels="$SurfaceAtlasDIR"/FreeSurferSubcorticalLabelTableLut.txt


echo -e "\n########## Surface processing settings ##########"
echo -e "HeadFolder: ${HeadFolder}"
echo -e "Subject:  ${Subject}"
echo -e "FreeSurferFolder: ${FreeSurferFolder}"
echo -e "SurfaceAtlasDIR: ${SurfaceAtlasDIR}"
echo -e "T1wFolder: ${T1wFolder}"
echo -e "AtlasSpaceFolder: ${AtlasSpaceFolder}"
echo -e "####################################################"


mkdir -p "$HeadFolder"
mkdir -p "$T1wFolder"
mkdir -p "$AtlasSpaceFolder"

echo -e "\n### Prepare data"
# T1 data
mri_convert "$FreeSurferFolder"/mri/orig.mgz "$T1wFolder"/temp.nii.gz
3dresample -orient RPI -prefix "$T1wFolder"/T1.nii.gz -inset "$T1wFolder"/temp.nii.gz
rm -rf "$T1wFolder"/temp.nii.gz
mri_convert "$FreeSurferFolder"/mri/T1.mgz "$T1wFolder"/temp.nii.gz
3dresample -orient RPI -prefix "$T1wFolder"/T1w_restore.nii.gz -inset "$T1wFolder"/temp.nii.gz
rm -rf "$T1wFolder"/temp.nii.gz
mri_convert "$FreeSurferFolder"/mri/brain.mgz "$T1wFolder"/temp.nii.gz
3dresample -orient RPI -prefix "$T1wFolder"/T1w_restore_brain.nii.gz -inset "$T1wFolder"/temp.nii.gz
rm -rf "$T1wFolder"/temp.nii.gz
# MNI template
fslmaths "$templateFolder"/"$templateName" "$AtlasSpaceFolder"/"$templateName"
fslmaths "$AtlasSpaceFolder"/"$templateName" -bin "$AtlasSpaceFolder"/"$templateName"_mask

echo -e "\n### Linear then non-linear registration from T1w_tal1mm to template"
mkdir "$AtlasSpaceFolder"/xfms

flirt -interp spline -dof 12 -in "$T1wFolder"/T1w_restore_brain.nii.gz -ref "$AtlasSpaceFolder"/"$templateName" -omat "$AtlasSpaceFolder"/xfms/T1w2TempLinear.mat -out "$AtlasSpaceFolder"/xfms/T1w2TempLinear.nii.gz
"$SurfaceAtlasDIR"/convert_mat_decimal.sh "$AtlasSpaceFolder"/xfms/T1w2TempLinear.mat
fnirt --in="$T1wFolder"/T1w_restore_brain.nii.gz --ref="$AtlasSpaceFolder"/"$templateName" --aff="$AtlasSpaceFolder"/xfms/T1w2TempLinear.mat_conv --refmask="$AtlasSpaceFolder"/"$templateName"_mask --fout="$AtlasSpaceFolder"/xfms/T1w2Temp.nii.gz --jout="$AtlasSpaceFolder"/xfms/NonlinearRegJacobians.nii.gz --refout="$AtlasSpaceFolder"/xfms/IntensityModulatedT1.nii.gz --iout="$AtlasSpaceFolder"/xfms/"$templatemm"Reg.nii.gz --logout="$AtlasSpaceFolder"/xfms/NonlinearReg.txt --intout="$AtlasSpaceFolder"/xfms/NonlinearIntensities.nii.gz --cout="$AtlasSpaceFolder"/xfms/NonlinearReg.nii.gz --config="$SurfaceAtlasDIR"/T1_2_Temp.cnf 
#fnirt --in="$T1wFolder"/T1w_restore_brain.nii.gz --ref="$AtlasSpaceFolder"/"$templateName" --aff="$AtlasSpaceFolder"/xfms/T1w2TempLinear.mat --refmask="$AtlasSpaceFolder"/"$templateName"_mask --fout="$AtlasSpaceFolder"/xfms/T1w2Temp.nii.gz --jout="$AtlasSpaceFolder"/xfms/NonlinearRegJacobians.nii.gz --refout="$AtlasSpaceFolder"/xfms/IntensityModulatedT1.nii.gz --iout="$AtlasSpaceFolder"/xfms/"$templatemm"Reg.nii.gz --logout="$AtlasSpaceFolder"/xfms/NonlinearReg.txt --intout="$AtlasSpaceFolder"/xfms/NonlinearIntensities.nii.gz --cout="$AtlasSpaceFolder"/xfms/NonlinearReg.nii.gz --config="$SurfaceAtlasDIR"/T1_2_Temp.cnf 
invwarp -w "$AtlasSpaceFolder"/xfms/T1w2Temp.nii.gz -o "$AtlasSpaceFolder"/xfms/T1w2TempInv.nii.gz -r "$AtlasSpaceFolder"/"$templateName"

echo -e "\n### Find c_ras offset between FreeSurfer surface and volume and generate matrix to transform surfaces"
MatrixX=`mri_info "$FreeSurferFolder"/mri/brain.finalsurfs.mgz | grep "c_r" | cut -d "=" -f 5 | sed s/" "/""/g`
MatrixY=`mri_info "$FreeSurferFolder"/mri/brain.finalsurfs.mgz | grep "c_a" | cut -d "=" -f 5 | sed s/" "/""/g`
MatrixZ=`mri_info "$FreeSurferFolder"/mri/brain.finalsurfs.mgz | grep "c_s" | cut -d "=" -f 5 | sed s/" "/""/g`
echo -e "1 0 0 ""$MatrixX" > "$FreeSurferFolder"/mri/c_ras.mat
echo -e "0 1 0 ""$MatrixY" >> "$FreeSurferFolder"/mri/c_ras.mat
echo -e "0 0 1 ""$MatrixZ" >> "$FreeSurferFolder"/mri/c_ras.mat
echo -e "0 0 0 1" >> "$FreeSurferFolder"/mri/c_ras.mat

echo -e "\n### Convert FreeSurfer Volumes (wmparc, aparc.a2009s+aseg, aparc+aseg)"
for Image in wmparc aparc.a2009s+aseg aparc+aseg ; do
  echo -e "    - $Image"
  mri_convert "$FreeSurferFolder"/mri/"$Image".mgz "$T1wFolder"/temp.nii.gz
  3dresample -orient RPI -prefix "$T1wFolder"/"$Image"_1mm.nii.gz -inset "$T1wFolder"/temp.nii.gz
  applywarp --rel --interp=nn -i "$T1wFolder"/"$Image"_1mm.nii.gz -r "$AtlasSpaceFolder"/xfms/"$templatemm"Reg.nii.gz --premat=$FSLDIR/etc/flirtsch/ident.mat -o "$AtlasSpaceFolder"/"$Image".nii.gz
  applywarp --rel --interp=nn -i "$T1wFolder"/"$Image"_1mm.nii.gz -r "$AtlasSpaceFolder"/xfms/"$templatemm"Reg.nii.gz -w "$AtlasSpaceFolder"/xfms/T1w2Temp.nii.gz -o "$AtlasSpaceFolder"/"$Image".nii.gz
  wb_command -volume-label-import "$T1wFolder"/"$Image"_1mm.nii.gz "$FreeSurferLabels" "$T1wFolder"/"$Image"_1mm.nii.gz -drop-unused-labels
  wb_command -volume-label-import "$AtlasSpaceFolder"/"$Image".nii.gz "$FreeSurferLabels" "$AtlasSpaceFolder"/"$Image".nii.gz -drop-unused-labels
done

echo -e "\n### Create FreeSurfer Brain Mask"
fslmaths "$T1wFolder"/T1w_restore_brain -bin "$T1wFolder"/brainmask_fs_1mm
fslmaths "$T1wFolder"/brainmask_fs_1mm -ero "$T1wFolder"/brainmask_fs_1mm
#applywarp --rel --interp=nn -i "$T1wFolder"/brainmask_fs_1mm.nii.gz -r "$AtlasSpaceFolder"/xfms/"$templatemm"Reg.nii.gz --premat="$AtlasSpaceFolder"/xfms/T1w2TempLinear.mat -o "$AtlasSpaceFolder"/brainmask_fs.nii.gz
applywarp --rel --interp=nn -i "$T1wFolder"/brainmask_fs_1mm.nii.gz -r "$AtlasSpaceFolder"/xfms/"$templatemm"Reg.nii.gz --premat="$AtlasSpaceFolder"/xfms/T1w2TempLinear.mat_conv -o "$AtlasSpaceFolder"/brainmask_fs.nii.gz

echo -e "\n### Add volume files to spec files"
mkdir "$T1wFolder"/Native
wb_command -add-to-spec-file "$T1wFolder"/Native/native.wb.spec INVALID "$T1wFolder"/T1w_restore_brain.nii.gz 
mkdir "$AtlasSpaceFolder"/Native
wb_command -add-to-spec-file "$AtlasSpaceFolder"/Native/native.wb.spec INVALID "$AtlasSpaceFolder"/xfms/"$templatemm"Reg.nii.gz 

echo -e "\n### Import Subcortical ROIs"
mkdir "$AtlasSpaceFolder"/ROIs
cp "$SurfaceAtlasDIR"/Atlas_ROIs."$templatemm".nii.gz "$AtlasSpaceFolder"/ROIs/Atlas_ROIs."$templatemm".nii.gz
applywarp --interp=nn -i "$AtlasSpaceFolder"/wmparc.nii.gz -r "$AtlasSpaceFolder"/ROIs/Atlas_ROIs."$templatemm".nii.gz -o "$AtlasSpaceFolder"/ROIs/wmparc."$templatemm".nii.gz
wb_command -volume-label-import "$AtlasSpaceFolder"/ROIs/wmparc."$templatemm".nii.gz "$FreeSurferLabels" "$AtlasSpaceFolder"/ROIs/wmparc."$templatemm".nii.gz -drop-unused-labels
applywarp --interp=nn -i "$SurfaceAtlasDIR"/Avgwmparc.nii.gz -r "$AtlasSpaceFolder"/ROIs/Atlas_ROIs."$templatemm".nii.gz -o "$AtlasSpaceFolder"/ROIs/Atlas_wmparc."$templatemm".nii.gz
wb_command -volume-label-import "$AtlasSpaceFolder"/ROIs/Atlas_wmparc."$templatemm".nii.gz "$FreeSurferLabels" "$AtlasSpaceFolder"/ROIs/Atlas_wmparc."$templatemm".nii.gz -drop-unused-labels
wb_command -volume-label-import "$AtlasSpaceFolder"/ROIs/wmparc."$templatemm".nii.gz "$SubcorticalGrayLabels" "$AtlasSpaceFolder"/ROIs/ROIs."$templatemm".nii.gz -discard-others
applywarp --interp=spline -i "$AtlasSpaceFolder"/xfms/"$templatemm"Reg.nii.gz -r "$AtlasSpaceFolder"/ROIs/Atlas_ROIs."$templatemm".nii.gz -o "$AtlasSpaceFolder"/xfms/"$templatemm"Reg."$templatemm".nii.gz

cp "$AtlasSpaceFolder"/xfms/"$templatemm"Reg.nii.gz "$AtlasSpaceFolder"/T1w_restore_brain.nii.gz
cp "$AtlasSpaceFolder"/xfms/"$templatemm"Reg."$templatemm".nii.gz "$AtlasSpaceFolder"/T1w_restore_brain."$templatemm".nii.gz

echo -e "\n### Start main native mesh processing"

echo -e "\n### Convert and volumetrically reigster white and pial surfaces making linear and nonlinear copies, add each to the appropriate spec file"
for Surface in white pial ; do
  echo -e "    - $Surface"

  echo -e "      - Left hemisphere"
  mris_convert "$FreeSurferFolder"/surf/lh."$Surface" "$T1wFolder"/Native/L."$Surface".native.surf.gii
  wb_command -set-structure "$T1wFolder"/Native/L."$Surface".native.surf.gii CORTEX_LEFT -surface-type ANATOMICAL -surface-secondary-type GRAY_WHITE
  wb_command -surface-apply-affine "$T1wFolder"/Native/L."$Surface".native.surf.gii "$FreeSurferFolder"/mri/c_ras.mat "$T1wFolder"/Native/L."$Surface".native.surf.gii
  wb_command -add-to-spec-file "$T1wFolder"/Native/native.wb.spec CORTEX_LEFT "$T1wFolder"/Native/L."$Surface".native.surf.gii 

#  wb_command -surface-apply-affine "$T1wFolder"/Native/L."$Surface".native.surf.gii "$AtlasSpaceFolder"/xfms/T1w2TempLinear.mat "$AtlasSpaceFolder"/Native/L."$Surface".native.surf.gii -flirt "$T1wFolder"/T1w_restore_brain.nii.gz "$AtlasSpaceFolder"/"$templateName".nii.gz 
  wb_command -surface-apply-affine "$T1wFolder"/Native/L."$Surface".native.surf.gii "$AtlasSpaceFolder"/xfms/T1w2TempLinear.mat_conv "$AtlasSpaceFolder"/Native/L."$Surface".native.surf.gii -flirt "$T1wFolder"/T1w_restore_brain.nii.gz "$AtlasSpaceFolder"/"$templateName".nii.gz 
#  wb_command -surface-apply-warpfield "$T1wFolder"/Native/L."$Surface".native.surf.gii "$AtlasSpaceFolder"/xfms/T1w2TempInv.nii.gz "$AtlasSpaceFolder"/Native/L."$Surface".native.surf.gii -fnirt "$AtlasSpaceFolder"/xfms/T1w2Temp.nii.gz 
  wb_command -add-to-spec-file "$AtlasSpaceFolder"/Native/native.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/Native/L."$Surface".native.surf.gii 

  echo -e "      - Right hemisphere"
  mris_convert "$FreeSurferFolder"/surf/rh."$Surface" "$T1wFolder"/Native/R."$Surface".native.surf.gii
  wb_command -set-structure "$T1wFolder"/Native/R."$Surface".native.surf.gii CORTEX_RIGHT -surface-type ANATOMICAL -surface-secondary-type GRAY_WHITE
  wb_command -surface-apply-affine "$T1wFolder"/Native/R."$Surface".native.surf.gii "$FreeSurferFolder"/mri/c_ras.mat "$T1wFolder"/Native/R."$Surface".native.surf.gii
  wb_command -add-to-spec-file "$T1wFolder"/Native/native.wb.spec CORTEX_RIGHT "$T1wFolder"/Native/R."$Surface".native.surf.gii 
  
#  wb_command -surface-apply-affine "$T1wFolder"/Native/R."$Surface".native.surf.gii "$AtlasSpaceFolder"/xfms/T1w2TempLinear.mat "$AtlasSpaceFolder"/Native/R."$Surface".native.surf.gii -flirt "$T1wFolder"/T1w_restore_brain.nii.gz "$AtlasSpaceFolder"/"$templateName".nii.gz
  wb_command -surface-apply-affine "$T1wFolder"/Native/R."$Surface".native.surf.gii "$AtlasSpaceFolder"/xfms/T1w2TempLinear.mat_conv "$AtlasSpaceFolder"/Native/R."$Surface".native.surf.gii -flirt "$T1wFolder"/T1w_restore_brain.nii.gz "$AtlasSpaceFolder"/"$templateName".nii.gz
#  wb_command -surface-apply-warpfield "$T1wFolder"/Native/R."$Surface".native.surf.gii "$AtlasSpaceFolder"/xfms/T1w2TempInv.nii.gz "$AtlasSpaceFolder"/Native/R."$Surface".native.surf.gii -fnirt "$AtlasSpaceFolder"/xfms/T1w2Temp.nii.gz 
  wb_command -add-to-spec-file "$AtlasSpaceFolder"/Native/native.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/Native/R."$Surface".native.surf.gii 
done

echo -e "\n### Create midthickness by averaging white and pial surfaces and use it to make inflated surfacess"
echo -e "### Get number of vertices from native file"
echo -e "### HCP fsaverage_LR32k used -iterations-scale 0.75. Compute new param value for native mesh density"

echo -e "    - Left, T1w"
# Create midthickness by averaging white and pial surfaces and use it to make inflated surfacess
wb_command -surface-average "$T1wFolder"/Native/L.midthickness.native.surf.gii -surf "$T1wFolder"/Native/L.white.native.surf.gii -surf "$T1wFolder"/Native/L.pial.native.surf.gii 
wb_command -set-structure "$T1wFolder"/Native/L.midthickness.native.surf.gii CORTEX_LEFT -surface-type ANATOMICAL -surface-secondary-type MIDTHICKNESS
wb_command -add-to-spec-file "$T1wFolder"/Native/native.wb.spec CORTEX_LEFT "$T1wFolder"/Native/L.midthickness.native.surf.gii
# Get number of vertices from native file
NativeVerts=`wb_command -file-information "$T1wFolder"/Native/L.midthickness.native.surf.gii | grep 'Number of Vertices:' | cut -f2 -d: | tr -d '[:space:]'`
# HCP fsaverage_LR32k used -iterations-scale 0.75. Compute new param value for native mesh density
NativeInflationScale=`echo -e "scale=4; 1 * 0.75 * $NativeVerts / 32492" | bc -l`
wb_command -surface-generate-inflated "$T1wFolder"/Native/L.midthickness.native.surf.gii "$T1wFolder"/Native/L.inflated.native.surf.gii "$T1wFolder"/Native/L.very_inflated.native.surf.gii -iterations-scale $NativeInflationScale
wb_command -add-to-spec-file "$T1wFolder"/Native/native.wb.spec CORTEX_LEFT "$T1wFolder"/Native/L.inflated.native.surf.gii 
wb_command -add-to-spec-file "$T1wFolder"/Native/native.wb.spec CORTEX_LEFT "$T1wFolder"/Native/L.very_inflated.native.surf.gii 

echo -e "    - Right, T1w"
# Create midthickness by averaging white and pial surfaces and use it to make inflated surfacess
wb_command -surface-average "$T1wFolder"/Native/R.midthickness.native.surf.gii -surf "$T1wFolder"/Native/R.white.native.surf.gii -surf "$T1wFolder"/Native/R.pial.native.surf.gii 
wb_command -set-structure "$T1wFolder"/Native/R.midthickness.native.surf.gii CORTEX_RIGHT -surface-type ANATOMICAL -surface-secondary-type MIDTHICKNESS
wb_command -add-to-spec-file "$T1wFolder"/Native/native.wb.spec CORTEX_RIGHT "$T1wFolder"/Native/R.midthickness.native.surf.gii
# Get number of vertices from native file
NativeVerts=`wb_command -file-information "$T1wFolder"/Native/R.midthickness.native.surf.gii | grep 'Number of Vertices:' | cut -f2 -d: | tr -d '[:space:]'`
# HCP fsaverage_LR32k used -iterations-scale 0.75. Compute new param value for native mesh density
NativeInflationScale=`echo -e "scale=4; 1 * 0.75 * $NativeVerts / 32492" | bc -l`
wb_command -surface-generate-inflated "$T1wFolder"/Native/R.midthickness.native.surf.gii "$T1wFolder"/Native/R.inflated.native.surf.gii "$T1wFolder"/Native/R.very_inflated.native.surf.gii -iterations-scale $NativeInflationScale
wb_command -add-to-spec-file "$T1wFolder"/Native/native.wb.spec CORTEX_LEFT "$T1wFolder"/Native/R.inflated.native.surf.gii 
wb_command -add-to-spec-file "$T1wFolder"/Native/native.wb.spec CORTEX_LEFT "$T1wFolder"/Native/R.very_inflated.native.surf.gii 

echo -e "    - Left, MNINonLinear"
# Create midthickness by averaging white and pial surfaces and use it to make inflated surfacess
wb_command -surface-average "$AtlasSpaceFolder"/Native/L.midthickness.native.surf.gii -surf "$AtlasSpaceFolder"/Native/L.white.native.surf.gii -surf "$AtlasSpaceFolder"/Native/L.pial.native.surf.gii 
wb_command -set-structure "$AtlasSpaceFolder"/Native/L.midthickness.native.surf.gii CORTEX_LEFT -surface-type ANATOMICAL -surface-secondary-type MIDTHICKNESS
wb_command -add-to-spec-file "$AtlasSpaceFolder"/Native/native.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/Native/L.midthickness.native.surf.gii
# Get number of vertices from native file
NativeVerts=`wb_command -file-information "$AtlasSpaceFolder"/Native/L.midthickness.native.surf.gii | grep 'Number of Vertices:' | cut -f2 -d: | tr -d '[:space:]'`
# HCP fsaverage_LR32k used -iterations-scale 0.75. Compute new param value for native mesh density
NativeInflationScale=`echo -e "scale=4; 1 * 0.75 * $NativeVerts / 32492" | bc -l`
wb_command -surface-generate-inflated "$AtlasSpaceFolder"/Native/L.midthickness.native.surf.gii "$AtlasSpaceFolder"/Native/L.inflated.native.surf.gii "$AtlasSpaceFolder"/Native/L.very_inflated.native.surf.gii -iterations-scale $NativeInflationScale
wb_command -add-to-spec-file "$AtlasSpaceFolder"/Native/native.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/Native/L.inflated.native.surf.gii 
wb_command -add-to-spec-file "$AtlasSpaceFolder"/Native/native.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/Native/L.very_inflated.native.surf.gii 

echo -e "    - Right, MNINonLinear"
# Create midthickness by averaging white and pial surfaces and use it to make inflated surfacess
wb_command -surface-average "$AtlasSpaceFolder"/Native/R.midthickness.native.surf.gii -surf "$AtlasSpaceFolder"/Native/R.white.native.surf.gii -surf "$AtlasSpaceFolder"/Native/R.pial.native.surf.gii 
wb_command -set-structure "$AtlasSpaceFolder"/Native/R.midthickness.native.surf.gii CORTEX_RIGHT -surface-type ANATOMICAL -surface-secondary-type MIDTHICKNESS
wb_command -add-to-spec-file "$AtlasSpaceFolder"/Native/native.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/Native/R.midthickness.native.surf.gii
# Get number of vertices from native file
NativeVerts=`wb_command -file-information "$AtlasSpaceFolder"/Native/R.midthickness.native.surf.gii | grep 'Number of Vertices:' | cut -f2 -d: | tr -d '[:space:]'`
# HCP fsaverage_LR32k used -iterations-scale 0.75. Compute new param value for native mesh density
NativeInflationScale=`echo -e "scale=4; 1 * 0.75 * $NativeVerts / 32492" | bc -l`
wb_command -surface-generate-inflated "$AtlasSpaceFolder"/Native/R.midthickness.native.surf.gii "$AtlasSpaceFolder"/Native/R.inflated.native.surf.gii "$AtlasSpaceFolder"/Native/R.very_inflated.native.surf.gii -iterations-scale $NativeInflationScale
wb_command -add-to-spec-file "$AtlasSpaceFolder"/Native/native.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/Native/R.inflated.native.surf.gii 
wb_command -add-to-spec-file "$AtlasSpaceFolder"/Native/native.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/Native/R.very_inflated.native.surf.gii 

echo -e "\n### Convert original and registered spherical surfaces and add them to the nonlinear spec file"
for Surface in sphere.reg sphere ; do
  echo -e "    - $Surface"

  echo -e "      - Left hemisphere"
  mris_convert "$FreeSurferFolder"/surf/lh."$Surface" "$AtlasSpaceFolder"/Native/L."$Surface".native.surf.gii
  wb_command -set-structure "$AtlasSpaceFolder"/Native/L."$Surface".native.surf.gii CORTEX_LEFT -surface-type SPHERICAL

  echo -e "      - Right hemisphere"
  mris_convert "$FreeSurferFolder"/surf/rh."$Surface" "$AtlasSpaceFolder"/Native/R."$Surface".native.surf.gii
  wb_command -set-structure "$AtlasSpaceFolder"/Native/R."$Surface".native.surf.gii CORTEX_RIGHT -surface-type SPHERICAL
done

wb_command -add-to-spec-file "$AtlasSpaceFolder"/Native/native.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/Native/L.sphere.native.surf.gii 
wb_command -add-to-spec-file "$AtlasSpaceFolder"/Native/native.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/Native/R.sphere.native.surf.gii 

echo -e "\n### Add more files to the spec file and convert other FreeSurfer surface data to metric/GIFTI including sulc, curv, and thickness"
for Map in sulc@sulc@Sulc thickness@thickness@Thickness curv@curvature@Curvature ; do
  echo -e "    - $Map"

  fsname=`echo $Map | cut -d "@" -f 1`
  wbname=`echo $Map | cut -d "@" -f 2`
  mapname=`echo $Map | cut -d "@" -f 3`

  echo -e "      - Left hemisphere"
  mris_convert -c "$FreeSurferFolder"/surf/lh."$fsname" "$FreeSurferFolder"/surf/lh.white "$AtlasSpaceFolder"/Native/L."$wbname".native.shape.gii
  wb_command -set-structure "$AtlasSpaceFolder"/Native/L."$wbname".native.shape.gii CORTEX_LEFT
  wb_command -metric-math "var * -1" "$AtlasSpaceFolder"/Native/L."$wbname".native.shape.gii -var var "$AtlasSpaceFolder"/Native/L."$wbname".native.shape.gii 
  wb_command -set-map-names "$AtlasSpaceFolder"/Native/L."$wbname".native.shape.gii -map 1 L_"$mapname"
  wb_command -metric-palette "$AtlasSpaceFolder"/Native/L."$wbname".native.shape.gii MODE_AUTO_SCALE_PERCENTAGE -pos-percent 2 98 -palette-name Gray_Interp -disp-pos true -disp-neg true -disp-zero true

  echo -e "      - Right hemisphere"
  mris_convert -c "$FreeSurferFolder"/surf/rh."$fsname" "$FreeSurferFolder"/surf/rh.white "$AtlasSpaceFolder"/Native/R."$wbname".native.shape.gii
  wb_command -set-structure "$AtlasSpaceFolder"/Native/R."$wbname".native.shape.gii CORTEX_RIGHT
  wb_command -metric-math "var * -1" "$AtlasSpaceFolder"/Native/R."$wbname".native.shape.gii -var var "$AtlasSpaceFolder"/Native/R."$wbname".native.shape.gii 
  wb_command -set-map-names "$AtlasSpaceFolder"/Native/R."$wbname".native.shape.gii -map 1 R_"$mapname"
  wb_command -metric-palette "$AtlasSpaceFolder"/Native/R."$wbname".native.shape.gii MODE_AUTO_SCALE_PERCENTAGE -pos-percent 2 98 -palette-name Gray_Interp -disp-pos true -disp-neg true -disp-zero true
done

echo -e "\n### Thickness specific operations"
for Hemisphere in L R ; do
  wb_command -metric-math "abs(thickness)" "$AtlasSpaceFolder"/Native/"$Hemisphere".thickness.native.shape.gii -var thickness "$AtlasSpaceFolder"/Native/"$Hemisphere".thickness.native.shape.gii
  wb_command -metric-palette "$AtlasSpaceFolder"/Native/"$Hemisphere".thickness.native.shape.gii MODE_AUTO_SCALE_PERCENTAGE -pos-percent 4 96 -interpolate true -palette-name videen_style -disp-pos true -disp-neg false -disp-zero false
  wb_command -metric-math "thickness > 0" "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii -var thickness "$AtlasSpaceFolder"/Native/"$Hemisphere".thickness.native.shape.gii
  wb_command -metric-fill-holes "$AtlasSpaceFolder"/Native/"$Hemisphere".midthickness.native.surf.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii
  wb_command -metric-remove-islands "$AtlasSpaceFolder"/Native/"$Hemisphere".midthickness.native.surf.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii
  wb_command -set-map-names "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii -map 1 "$Hemisphere"_ROI
  wb_command -metric-dilate "$AtlasSpaceFolder"/Native/"$Hemisphere".thickness.native.shape.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".midthickness.native.surf.gii 10 "$AtlasSpaceFolder"/Native/"$Hemisphere".thickness.native.shape.gii -nearest
  wb_command -metric-dilate "$AtlasSpaceFolder"/Native/"$Hemisphere".curvature.native.shape.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".midthickness.native.surf.gii 10 "$AtlasSpaceFolder"/Native/"$Hemisphere".curvature.native.shape.gii -nearest
done

echo -e "\n### Label operations"
for Map in aparc aparc.a2009s BA_exvivo ; do
  echo -e "    - $Map"

  echo -e "      - Left hemisphere"
  mris_convert --annot "$FreeSurferFolder"/label/lh."$Map".annot "$FreeSurferFolder"/surf/lh.white "$AtlasSpaceFolder"/Native/L."$Map".native.label.gii
  wb_command -set-structure "$AtlasSpaceFolder"/Native/L."$Map".native.label.gii CORTEX_LEFT
  wb_command -set-map-names "$AtlasSpaceFolder"/Native/L."$Map".native.label.gii -map 1 L_"$Map"
  wb_command -gifti-label-add-prefix "$AtlasSpaceFolder"/Native/L."$Map".native.label.gii L_ "$AtlasSpaceFolder"/Native/L."$Map".native.label.gii 

  echo -e "      - Right hemisphere"
  mris_convert --annot "$FreeSurferFolder"/label/rh."$Map".annot "$FreeSurferFolder"/surf/rh.white "$AtlasSpaceFolder"/Native/R."$Map".native.label.gii
  wb_command -set-structure "$AtlasSpaceFolder"/Native/R."$Map".native.label.gii CORTEX_RIGHT
  wb_command -set-map-names "$AtlasSpaceFolder"/Native/R."$Map".native.label.gii -map 1 R_"$Map"
  wb_command -gifti-label-add-prefix "$AtlasSpaceFolder"/Native/R."$Map".native.label.gii R_ "$AtlasSpaceFolder"/Native/R."$Map".native.label.gii 
done

echo -e "\n### End main native mesh processing"




echo -e "\n### HighResMesh"
mkdir -p "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k
#Copy Atlas Files
cp "$SurfaceAtlasDIR"/fs_L/fsaverage.L.sphere."$HighResMesh"k_fs_L.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.sphere."$HighResMesh"k_fs_L.surf.gii
cp "$SurfaceAtlasDIR"/fs_L/fs_L-to-fs_LR_fsaverage.L_LR.spherical_std."$HighResMesh"k_fs_L.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.def_sphere."$HighResMesh"k_fs_L.surf.gii
cp "$SurfaceAtlasDIR"/fsaverage.L_LR.spherical_std."$HighResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.sphere."$HighResMesh"k_fs_LR.surf.gii
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$HighResMesh"k_fs_LR.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.sphere."$HighResMesh"k_fs_LR.surf.gii
cp "$SurfaceAtlasDIR"/L.atlasroi."$HighResMesh"k_fs_LR.shape.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.atlasroi."$HighResMesh"k_fs_LR.shape.gii
cp "$SurfaceAtlasDIR"/L.refsulc."$HighResMesh"k_fs_LR.shape.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.refsulc."$HighResMesh"k_fs_LR.shape.gii
if [ -e "$SurfaceAtlasDIR"/colin.cerebral.L.flat."$HighResMesh"k_fs_LR.surf.gii ] ; then
  cp "$SurfaceAtlasDIR"/colin.cerebral.L.flat."$HighResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.flat."$HighResMesh"k_fs_LR.surf.gii
  wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$HighResMesh"k_fs_LR.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.flat."$HighResMesh"k_fs_LR.surf.gii
fi

cp "$SurfaceAtlasDIR"/fs_R/fsaverage.R.sphere."$HighResMesh"k_fs_R.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.sphere."$HighResMesh"k_fs_R.surf.gii
cp "$SurfaceAtlasDIR"/fs_R/fs_R-to-fs_LR_fsaverage.R_LR.spherical_std."$HighResMesh"k_fs_R.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.def_sphere."$HighResMesh"k_fs_R.surf.gii
cp "$SurfaceAtlasDIR"/fsaverage.L_LR.spherical_std."$HighResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.sphere."$HighResMesh"k_fs_LR.surf.gii
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$HighResMesh"k_fs_LR.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.sphere."$HighResMesh"k_fs_LR.surf.gii
cp "$SurfaceAtlasDIR"/R.atlasroi."$HighResMesh"k_fs_LR.shape.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.atlasroi."$HighResMesh"k_fs_LR.shape.gii
cp "$SurfaceAtlasDIR"/R.refsulc."$HighResMesh"k_fs_LR.shape.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.refsulc."$HighResMesh"k_fs_LR.shape.gii
if [ -e "$SurfaceAtlasDIR"/colin.cerebral.R.flat."$HighResMesh"k_fs_LR.surf.gii ] ; then
  cp "$SurfaceAtlasDIR"/colin.cerebral.R.flat."$HighResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.flat."$HighResMesh"k_fs_LR.surf.gii
  wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$HighResMesh"k_fs_LR.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.flat."$HighResMesh"k_fs_LR.surf.gii
fi

echo -e "\n### Concatinate FS registration to FS --> FS_LR registration"
for Hemisphere in L R; do
  echo -e "    - $Hemisphere"
  wb_command -surface-sphere-project-unproject "$AtlasSpaceFolder"/Native/"$Hemisphere".sphere.reg.native.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".sphere."$HighResMesh"k_fs_"$Hemisphere".surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".def_sphere."$HighResMesh"k_fs_"$Hemisphere".surf.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".sphere.reg.reg_LR.native.surf.gii
done

echo -e "\n### Make FreeSurfer Registration Areal Distortion Maps"
for Hemisphere in L R; do
  echo -e "    - $Hemisphere"

  wb_command -surface-vertex-areas "$AtlasSpaceFolder"/Native/"$Hemisphere".sphere.native.surf.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".sphere.native.shape.gii
  wb_command -surface-vertex-areas "$AtlasSpaceFolder"/Native/"$Hemisphere".sphere.reg.reg_LR.native.surf.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".sphere.reg.reg_LR.native.shape.gii
  wb_command -metric-math "ln(spherereg / sphere) / ln(2)" "$AtlasSpaceFolder"/Native/"$Hemisphere".ArealDistortion_FS.native.shape.gii -var sphere "$AtlasSpaceFolder"/Native/"$Hemisphere".sphere.native.shape.gii -var spherereg "$AtlasSpaceFolder"/Native/"$Hemisphere".sphere.reg.reg_LR.native.shape.gii
  rm -rf "$AtlasSpaceFolder"/Native/"$Hemisphere".sphere.native.shape.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".sphere.reg.reg_LR.native.shape.gii
  wb_command -set-map-names "$AtlasSpaceFolder"/Native/"$Hemisphere".ArealDistortion_FS.native.shape.gii -map 1 "$Hemisphere"_Areal_Distortion_FS
  wb_command -metric-palette "$AtlasSpaceFolder"/Native/"$Hemisphere".ArealDistortion_FS.native.shape.gii MODE_AUTO_SCALE -palette-name ROY-BIG-BL -thresholding THRESHOLD_TYPE_NORMAL THRESHOLD_TEST_SHOW_OUTSIDE -1 1
done

echo -e "\n### Ensure no zeros in atlas medial wall ROI"
for Hemisphere in L R; do
  echo -e "    - $Hemisphere"

  RegSphere="${AtlasSpaceFolder}/Native/${Hemisphere}.sphere.reg.reg_LR.native.surf.gii"
  wb_command -metric-resample "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".atlasroi."$HighResMesh"k_fs_LR.shape.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".sphere."$HighResMesh"k_fs_LR.surf.gii ${RegSphere} BARYCENTRIC "$AtlasSpaceFolder"/Native/"$Hemisphere".atlasroi.native.shape.gii -largest
  wb_command -metric-math "(atlas + individual) > 0" "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii -var atlas "$AtlasSpaceFolder"/Native/"$Hemisphere".atlasroi.native.shape.gii -var individual "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii
  wb_command -metric-mask "$AtlasSpaceFolder"/Native/"$Hemisphere".thickness.native.shape.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".thickness.native.shape.gii
  wb_command -metric-mask "$AtlasSpaceFolder"/Native/"$Hemisphere".curvature.native.shape.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii "$AtlasSpaceFolder"/Native/"$Hemisphere".curvature.native.shape.gii
done

echo -e "\n### Populate Highres fs_LR spec file"
#Deform surfaces and other data according to native to folding-based registration selected above.  Regenerate inflated surfaces.
for Surface in white midthickness pial ; do
  echo -e "    - $Surface"

  wb_command -surface-resample "$AtlasSpaceFolder"/Native/L."$Surface".native.surf.gii "$AtlasSpaceFolder"/Native/L.sphere.reg.reg_LR.native.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.sphere."$HighResMesh"k_fs_LR.surf.gii BARYCENTRIC "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L."$Surface"."$HighResMesh"k_fs_LR.surf.gii
  wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$HighResMesh"k_fs_LR.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L."$Surface"."$HighResMesh"k_fs_LR.surf.gii
  wb_command -surface-resample "$AtlasSpaceFolder"/Native/R."$Surface".native.surf.gii "$AtlasSpaceFolder"/Native/R.sphere.reg.reg_LR.native.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.sphere."$HighResMesh"k_fs_LR.surf.gii BARYCENTRIC "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R."$Surface"."$HighResMesh"k_fs_LR.surf.gii
  wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$HighResMesh"k_fs_LR.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R."$Surface"."$HighResMesh"k_fs_LR.surf.gii
done
wb_command -surface-generate-inflated "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.midthickness."$HighResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.inflated."$HighResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.very_inflated."$HighResMesh"k_fs_LR.surf.gii -iterations-scale 2.5
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$HighResMesh"k_fs_LR.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.inflated."$HighResMesh"k_fs_LR.surf.gii
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$HighResMesh"k_fs_LR.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/L.very_inflated."$HighResMesh"k_fs_LR.surf.gii
wb_command -surface-generate-inflated "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.midthickness."$HighResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.inflated."$HighResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.very_inflated."$HighResMesh"k_fs_LR.surf.gii -iterations-scale 2.5
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$HighResMesh"k_fs_LR.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.inflated."$HighResMesh"k_fs_LR.surf.gii
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$HighResMesh"k_fs_LR.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/R.very_inflated."$HighResMesh"k_fs_LR.surf.gii

for Hemisphere in L R; do
  echo -e "    - $Hemisphere"
  for Map in thickness curvature ; do
    echo -e "      - $Map"
    RegSphere="${AtlasSpaceFolder}/Native/${Hemisphere}.sphere.reg.reg_LR.native.surf.gii"
    wb_command -metric-resample "$AtlasSpaceFolder"/Native/"$Hemisphere"."$Map".native.shape.gii ${RegSphere} "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".sphere."$HighResMesh"k_fs_LR.surf.gii ADAP_BARY_AREA "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere"."$Map"."$HighResMesh"k_fs_LR.shape.gii -area-surfs "$T1wFolder"/Native/"$Hemisphere".midthickness.native.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".midthickness."$HighResMesh"k_fs_LR.surf.gii -current-roi "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii
    wb_command -metric-mask "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere"."$Map"."$HighResMesh"k_fs_LR.shape.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".atlasroi."$HighResMesh"k_fs_LR.shape.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere"."$Map"."$HighResMesh"k_fs_LR.shape.gii
  done  
  wb_command -metric-resample "$AtlasSpaceFolder"/Native/"$Hemisphere".ArealDistortion_FS.native.shape.gii ${RegSphere} "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".sphere."$HighResMesh"k_fs_LR.surf.gii ADAP_BARY_AREA "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".ArealDistortion_FS."$HighResMesh"k_fs_LR.shape.gii -area-surfs "$T1wFolder"/Native/"$Hemisphere".midthickness.native.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".midthickness."$HighResMesh"k_fs_LR.surf.gii
  wb_command -metric-resample "$AtlasSpaceFolder"/Native/"$Hemisphere".sulc.native.shape.gii ${RegSphere} "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".sphere."$HighResMesh"k_fs_LR.surf.gii ADAP_BARY_AREA "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".sulc."$HighResMesh"k_fs_LR.shape.gii -area-surfs "$T1wFolder"/Native/"$Hemisphere".midthickness.native.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".midthickness."$HighResMesh"k_fs_LR.surf.gii

  for Map in aparc aparc.a2009s ; do
    echo -e "      - $Map"
    #if [ -e "$FreeSurferFolder"/label/"$hemisphere"h."$Map".annot ] ; then
      wb_command -label-resample "$AtlasSpaceFolder"/Native/"$Hemisphere"."$Map".native.label.gii ${RegSphere} "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere".sphere."$HighResMesh"k_fs_LR.surf.gii BARYCENTRIC "$AtlasSpaceFolder"/fsaverage_LR"$HighResMesh"k/"$Hemisphere"."$Map"."$HighResMesh"k_fs_LR.label.gii -largest
    #fi
  done
done


echo -e "\n### LowResMesh"
mkdir -p "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k
#Copy Atlas Files
cp "$SurfaceAtlasDIR"/L.sphere."$LowResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L.sphere."$LowResMesh"k_fs_LR.surf.gii
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$LowResMesh"k_fs_LR.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L.sphere."$LowResMesh"k_fs_LR.surf.gii
cp "$SurfaceAtlasDIR"/L.atlasroi."$LowResMesh"k_fs_LR.shape.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L.atlasroi."$LowResMesh"k_fs_LR.shape.gii
if [ -e "$SurfaceAtlasDIR"/colin.cerebral.L.flat."$LowResMesh"k_fs_LR.surf.gii ] ; then
  cp "$SurfaceAtlasDIR"/colin.cerebral.L.flat."$LowResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L.flat."$LowResMesh"k_fs_LR.surf.gii
  wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$LowResMesh"k_fs_LR.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L.flat."$LowResMesh"k_fs_LR.surf.gii
fi
cp "$SurfaceAtlasDIR"/R.sphere."$LowResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R.sphere."$LowResMesh"k_fs_LR.surf.gii
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$LowResMesh"k_fs_LR.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R.sphere."$LowResMesh"k_fs_LR.surf.gii
cp "$SurfaceAtlasDIR"/R.atlasroi."$LowResMesh"k_fs_LR.shape.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R.atlasroi."$LowResMesh"k_fs_LR.shape.gii
if [ -e "$SurfaceAtlasDIR"/colin.cerebral.R.flat."$LowResMesh"k_fs_LR.surf.gii ] ; then
  cp "$SurfaceAtlasDIR"/colin.cerebral.R.flat."$LowResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R.flat."$LowResMesh"k_fs_LR.surf.gii
  wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$LowResMesh"k_fs_LR.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R.flat."$LowResMesh"k_fs_LR.surf.gii
fi

echo -e "\n### Create downsampled fs_LR spec files"
for Surface in white midthickness pial ; do
  echo -e "    - $Surface"

  wb_command -surface-resample "$AtlasSpaceFolder"/Native/L."$Surface".native.surf.gii "$AtlasSpaceFolder"/Native/L.sphere.reg.reg_LR.native.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L.sphere."$LowResMesh"k_fs_LR.surf.gii BARYCENTRIC "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L."$Surface"."$LowResMesh"k_fs_LR.surf.gii
  wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$LowResMesh"k_fs_LR.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L."$Surface"."$LowResMesh"k_fs_LR.surf.gii
  wb_command -surface-resample "$AtlasSpaceFolder"/Native/R."$Surface".native.surf.gii "$AtlasSpaceFolder"/Native/R.sphere.reg.reg_LR.native.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R.sphere."$LowResMesh"k_fs_LR.surf.gii BARYCENTRIC "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R."$Surface"."$LowResMesh"k_fs_LR.surf.gii
  wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$LowResMesh"k_fs_LR.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R."$Surface"."$LowResMesh"k_fs_LR.surf.gii
done
wb_command -surface-generate-inflated "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L.midthickness."$LowResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L.inflated."$LowResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L.very_inflated."$LowResMesh"k_fs_LR.surf.gii -iterations-scale 0.75
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$LowResMesh"k_fs_LR.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L.inflated."$LowResMesh"k_fs_LR.surf.gii
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$LowResMesh"k_fs_LR.wb.spec CORTEX_LEFT "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/L.very_inflated."$LowResMesh"k_fs_LR.surf.gii
wb_command -surface-generate-inflated "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R.midthickness."$LowResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R.inflated."$LowResMesh"k_fs_LR.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R.very_inflated."$LowResMesh"k_fs_LR.surf.gii -iterations-scale 0.75
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$LowResMesh"k_fs_LR.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R.inflated."$LowResMesh"k_fs_LR.surf.gii
wb_command -add-to-spec-file "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$LowResMesh"k_fs_LR.wb.spec CORTEX_RIGHT "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/R.very_inflated."$LowResMesh"k_fs_LR.surf.gii

for Hemisphere in L R; do
  echo -e "    - $Hemisphere"
  RegSphere="${AtlasSpaceFolder}/Native/${Hemisphere}.sphere.reg.reg_LR.native.surf.gii"
  for Map in sulc thickness curvature ; do
    echo -e "      - $Map"
    wb_command -metric-resample "$AtlasSpaceFolder"/Native/"$Hemisphere"."$Map".native.shape.gii ${RegSphere} "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere".sphere."$LowResMesh"k_fs_LR.surf.gii ADAP_BARY_AREA "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere"."$Map"."$LowResMesh"k_fs_LR.shape.gii -area-surfs "$T1wFolder"/Native/"$Hemisphere".midthickness.native.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere".midthickness."$LowResMesh"k_fs_LR.surf.gii -current-roi "$AtlasSpaceFolder"/Native/"$Hemisphere".roi.native.shape.gii
    wb_command -metric-mask "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere"."$Map"."$LowResMesh"k_fs_LR.shape.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere".atlasroi."$LowResMesh"k_fs_LR.shape.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere"."$Map"."$LowResMesh"k_fs_LR.shape.gii
  done  
  wb_command -metric-resample "$AtlasSpaceFolder"/Native/"$Hemisphere".ArealDistortion_FS.native.shape.gii ${RegSphere} "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere".sphere."$LowResMesh"k_fs_LR.surf.gii ADAP_BARY_AREA "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere".ArealDistortion_FS."$LowResMesh"k_fs_LR.shape.gii -area-surfs "$T1wFolder"/Native/"$Hemisphere".midthickness.native.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere".midthickness."$LowResMesh"k_fs_LR.surf.gii
  wb_command -metric-resample "$AtlasSpaceFolder"/Native/"$Hemisphere".sulc.native.shape.gii ${RegSphere} "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere".sphere."$LowResMesh"k_fs_LR.surf.gii ADAP_BARY_AREA "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere".sulc."$LowResMesh"k_fs_LR.shape.gii -area-surfs "$T1wFolder"/Native/"$Hemisphere".midthickness.native.surf.gii "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere".midthickness."$LowResMesh"k_fs_LR.surf.gii

  for Map in aparc aparc.a2009s ; do
    echo -e "      - $Map"
    #if [ -e "$FreeSurferFolder"/label/"$hemisphere"h."$Map".annot ] ; then
      wb_command -label-resample "$AtlasSpaceFolder"/Native/"$Hemisphere"."$Map".native.label.gii ${RegSphere} "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere".sphere."$LowResMesh"k_fs_LR.surf.gii BARYCENTRIC "$AtlasSpaceFolder"/fsaverage_LR"$LowResMesh"k/"$Hemisphere"."$Map"."$LowResMesh"k_fs_LR.label.gii -largest
    #fi
  done
done

