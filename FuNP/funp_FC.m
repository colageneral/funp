function varargout = funp_FC(varargin)
% FUNP_FC MATLAB code for funp_FC.fig
%      FUNP_FC, by itself, creates a new FUNP_FC or raises the existing
%      singleton*.
%
%      H = FUNP_FC returns the handle to a new FUNP_FC or the handle to
%      the existing singleton*.
%
%      FUNP_FC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FUNP_FC.M with the given input arguments.
%
%      FUNP_FC('Property','Value',...) creates a new FUNP_FC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before funp_FC_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to funp_FC_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help funp_FC

% Last Modified by GUIDE v2.5 18-Nov-2018 23:52:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @funp_FC_OpeningFcn, ...
                   'gui_OutputFcn',  @funp_FC_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before funp_FC is made visible.
function funp_FC_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to funp_FC (see VARARGIN)

% Choose default command line output for funp_FC
handles.output = hObject;

FindPath = which('funp');
if isunix == 1
    temp = strsplit(FindPath,'/');
elseif ismac == 1
    temp = strsplit(FindPath,'/');
elseif ispc == 1
    temp = strsplit(FindPath,'\');
end
HeadPath = [];
for i = 1:length(temp)-1
    HeadPath = strcat([HeadPath,temp{i},'/']);
end

axes(handles.logo)
ml = imread(strcat(HeadPath,'/pictures/MainLogo_FC.png'));
imshow(ml);
axis off;

axes(handles.StaticPic)
ml = imread(strcat(HeadPath,'/pictures/Static.png'));
imshow(ml);
axis off;
axes(handles.DynamicPic)
ml = imread(strcat(HeadPath,'/pictures/Dynamic.png'));
imshow(ml);
axis off;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes funp_FC wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = funp_FC_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Static.
function Static_Callback(hObject, eventdata, handles)
% hObject    handle to Static (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_funp_FC_static = funp_FC_static;
handles.h_funp_FC_static = guihandles(h_funp_FC_static);
guidata(hObject, handles);


% --- Executes on button press in Dynamic.
function Dynamic_Callback(hObject, eventdata, handles)
% hObject    handle to Dynamic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_funp_FC_dynamic = funp_FC_dynamic;
handles.h_funp_FC_dynamic = guihandles(h_funp_FC_dynamic);
guidata(hObject, handles);
