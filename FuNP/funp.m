function varargout = funp(varargin)
% FUNP MATLAB code for funp.fig
%      FUNP, by itself, creates a new FUNP or raises the existing
%      singleton*.
%
%      H = FUNP returns the handle to a new FUNP or the handle to
%      the existing singleton*.
%
%      FUNP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FUNP.M with the given input arguments.
%
%      FUNP('Property','Value',...) creates a new FUNP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before funp_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to funp_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help funp

% Last Modified by GUIDE v2.5 11-May-2018 13:28:02

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @funp_OpeningFcn, ...
                   'gui_OutputFcn',  @funp_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before funp is made visible.
function funp_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to funp (see VARARGIN)

% Choose default command line output for funp
handles.output = hObject;

FindPath = which('funp');
if isunix == 1
    temp = strsplit(FindPath,'/');
elseif ismac == 1
    temp = strsplit(FindPath,'/');
elseif ispc == 1
    temp = strsplit(FindPath,'\');
end
HeadPath = [];
for i = 1:length(temp)-1
    HeadPath = strcat([HeadPath,temp{i},'/']);
end

axes(handles.logo)
ml = imread(strcat(HeadPath,'/pictures/MainLogo.png'));
imshow(ml);
axis off;

axes(handles.VolPic)
ml = imread(strcat(HeadPath,'/pictures/Volume.png'));
imshow(ml);
axis off;
axes(handles.SurfPic)
ml = imread(strcat(HeadPath,'/pictures/Surface.png'));
imshow(ml);
axis off;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes funp wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = funp_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Volume_based.
function Volume_based_Callback(hObject, eventdata, handles)
% hObject    handle to Volume_based (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Volume = Volume;
handles.h_Volume = guihandles(h_Volume);
guidata(hObject, handles);


% --- Executes on button press in Surface_based.
function Surface_based_Callback(hObject, eventdata, handles)
% hObject    handle to Surface_based (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Surface = Surface;
handles.h_Surface = guihandles(h_Surface);
guidata(hObject, handles);


% --- Executes when uibuttongroup1 is resized.
function uibuttongroup1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to uibuttongroup1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
