function preproc_surface_T1(T1_total_list, standard_path, standard_name, standard_ext, standard_mm)
clc

FindPath = which('funp');
if isunix == 1
    temp = strsplit(FindPath,'/');
elseif ismac == 1
    temp = strsplit(FindPath,'/');
elseif ispc == 1
    temp = strsplit(FindPath,'\');
end
HeadPath = [];
for i = 1:length(temp)-1
    HeadPath = strcat([HeadPath,temp{i},'/']);
end
SurfAtlasPath = [];
for i = 1:length(temp)-2
    SurfAtlasPath = strcat([SurfAtlasPath,temp{i},'/']);
end
SurfAtlasPath = strcat(SurfAtlasPath,'SurfaceAtlas');

disp('%%%%%%%%%%%%%%% Settings %%%%%%%%%%%%%%%')
num_subj = size(T1_total_list,1);
disp(strcat(['### Number of subjects: ',int2str(num_subj)]));
template = strcat(standard_path,standard_name,standard_ext);
disp(strcat(['### Standard data: ',template]));
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')

% Save settings
if isunix == 1
    temp = strsplit(T1_total_list(1,:),'/');
elseif ismac == 1
    temp = strsplit(T1_total_list(1,:),'/');
elseif ispc == 1
    temp = strsplit(T1_total_list(1,:),'\');
end
anat_path = [];
for i = 1:length(temp)-1
    anat_path = strcat([anat_path,temp{i},'/']);
end
cd(anat_path);
cd('../');
% save('FuNP_Surface_T1_settings.mat', 'T1_total_list', 'standard_path', 'standard_name', 'standard_ext', 'standard_mm');

for list = 1:num_subj
    if isunix == 1
        temp = strsplit(T1_total_list(list,:),'/');
    elseif ismac == 1
        temp = strsplit(T1_total_list(list,:),'/');
    elseif ispc == 1
        temp = strsplit(T1_total_list(list,:),'\');
    end
    anat_path = [];
    for i = 1:length(temp)-1
        anat_path = strcat([anat_path,temp{i},'/']);
    end
    temp2 = strsplit(temp{end},'.');
    anat_name = temp2{1};
    if length(temp2) > 2
        anat_ext = strcat('.',temp2{2},'.',temp2{3});
    elseif length(temp2) == 2
        anat_ext = strcat('.',temp2{2});
    end
    
    disp(' ');
    disp('%%%%% T1 recon-all start %%%%%');
    system(strcat(['recon-all -i ',anat_path,anat_name,anat_ext,' -sd ',anat_path,' -s fs_initial -all']));
    disp(' ');
    disp('%%%%% T1 recon-all finished %%%%%');
    
    disp(' ');
    disp('%%%%% T1 surface adjustment start %%%%%');
    system(strcat([HeadPath,'/Surface_structural.sh ',anat_path,' wb_adjust ',anat_path,'fs_initial ',SurfAtlasPath,' ',standard_path,' ',standard_name,' ',int2str(standard_mm),'mm']));
    disp(' ');
    disp('%%%%% T1 surface adjustment finished %%%%%');

    disp(' ');
    if isunix == 1
        system(strcat(['chmod 777 -R ',anat_path]));
    elseif ismac == 1
        system(strcat(['chmod -R 777 ',anat_path]));
    elseif ispc == 1
        system(strcat(['chmod 777 -R ',anat_path]));
    end
end
