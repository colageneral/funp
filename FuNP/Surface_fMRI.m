function varargout = Surface_fMRI(varargin)
% SURFACE_FMRI MATLAB code for Surface_fMRI.fig
%      SURFACE_FMRI, by itself, creates a new SURFACE_FMRI or raises the existing
%      singleton*.
%
%      H = SURFACE_FMRI returns the handle to a new SURFACE_FMRI or the handle to
%      the existing singleton*.
%
%      SURFACE_FMRI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SURFACE_FMRI.M with the given input arguments.
%
%      SURFACE_FMRI('Property','Value',...) creates a new SURFACE_FMRI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Surface_fMRI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Surface_fMRI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Surface_fMRI

% Last Modified by GUIDE v2.5 16-Feb-2019 15:00:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Surface_fMRI_OpeningFcn, ...
                   'gui_OutputFcn',  @Surface_fMRI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Surface_fMRI is made visible.
function Surface_fMRI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Surface_fMRI (see VARARGIN)

% Choose default command line output for Surface_fMRI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Surface_fMRI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Surface_fMRI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in load_wb.
function load_wb_Callback(hObject, eventdata, handles)
% hObject    handle to load_wb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Loadwb = Loadwb;
handles.h_Loadwb = guihandles(h_Loadwb);
guidata(hObject, handles);



% --- Executes on button press in load_fMRI.
function load_fMRI_Callback(hObject, eventdata, handles)
% hObject    handle to load_fMRI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_LoadfMRI = LoadfMRI;
handles.h_LoadfMRI = guihandles(h_LoadfMRI);
guidata(hObject, handles);


% --- Executes on button press in Reorient.
function Reorient_Callback(hObject, eventdata, handles)
% hObject    handle to Reorient (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Reorient = Reorient;
handles.h_Reorient = guihandles(h_Reorient);
guidata(hObject, handles);


% --- Executes on button press in delNvol.
function delNvol_Callback(hObject, eventdata, handles)
% hObject    handle to delNvol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_delNvol = delNvol;
handles.h_delNvol = guihandles(h_delNvol);
guidata(hObject, handles);


% --- Executes on button press in MotScrub.
function MotScrub_Callback(hObject, eventdata, handles)
% hObject    handle to MotScrub (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_MotScrub = MotScrub;
handles.h_MotScrub = guihandles(h_MotScrub);
guidata(hObject, handles);


% --- Executes on button press in MotCor.
function MotCor_Callback(hObject, eventdata, handles)
% hObject    handle to MotCor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_MotCor = MotCor;
handles.h_MotCor = guihandles(h_MotCor);
guidata(hObject, handles);


% --- Executes on button press in STcor.
function STcor_Callback(hObject, eventdata, handles)
% hObject    handle to STcor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_STCor = STcor;
handles.h_STCor = guihandles(h_STCor);
guidata(hObject, handles);


% --- Executes on button press in DistCor.
function DistCor_Callback(hObject, eventdata, handles)
% hObject    handle to DistCor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_DistCor = DistCor;
handles.h_DistCor = guihandles(h_DistCor);
guidata(hObject, handles);


% --- Executes on button press in SkullRemoval.
function SkullRemoval_Callback(hObject, eventdata, handles)
% hObject    handle to SkullRemoval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_SkullRemoval_fMRI = SkullRemoval_fMRI;
handles.SkullRemoval_fMRI = guihandles(SkullRemoval_fMRI);
guidata(hObject, handles);


% --- Executes on button press in IntNorm.
function IntNorm_Callback(hObject, eventdata, handles)
% hObject    handle to IntNorm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_IntNorm = IntNorm;
handles.h_IntNorm = guihandles(h_IntNorm);
guidata(hObject, handles);


% --- Executes on button press in Registration.
function Registration_Callback(hObject, eventdata, handles)
% hObject    handle to Registration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Registration_fMRI = Registration_fMRI;
handles.h_Registration_fMRI = guihandles(h_Registration_fMRI);
guidata(hObject, handles);


% --- Executes on button press in NVremove.
function NVremove_Callback(hObject, eventdata, handles)
% hObject    handle to NVremove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_NVremove = NVremove;
handles.h_NVremove = guihandles(h_NVremove);
guidata(hObject, handles);


% --- Executes on button press in TempFilt.
function TempFilt_Callback(hObject, eventdata, handles)
% hObject    handle to TempFilt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_TempFilt = TempFilt;
handles.h_TempFilt = guihandles(h_TempFilt);
guidata(hObject, handles);


% --- Executes on button press in Smooth.
function Smooth_Callback(hObject, eventdata, handles)
% hObject    handle to Smooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h_Smooth = Smooth;
handles.h_Smooth = guihandles(h_Smooth);
guidata(hObject, handles);


% --- Executes on button press in GO.
function GO_Callback(hObject, eventdata, handles)
% hObject    handle to GO (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
preproc_surface_fMRI(evalin('base','wb_folder'), evalin('base','fMRI_total_list'), evalin('base','reorient'), evalin('base','orientation'),...
    evalin('base','delnvol'), evalin('base','VOLorSEC'), evalin('base','VOLorSEC_enterval'),...
    evalin('base','motscrub'), evalin('base','FDthresh_enterval'), evalin('base','motcor'),...
    evalin('base','stcor'), evalin('base','stcor2'), evalin('base','stcor_opt'), evalin('base','slice_order_file'),...
    evalin('base','discor'), evalin('base','reverse_path'), evalin('base','reverse_name'), evalin('base','reverse_ext'),...
    evalin('base','total_readout_time'), evalin('base','skullremoval_fMRI'), evalin('base','intnorm'),...
    evalin('base','regis_fMRI'), evalin('base','T1_total_list'), evalin('base','dof1'),...
    evalin('base','standard_path'), evalin('base','standard_name'), evalin('base','standard_ext'), evalin('base','dof2'),...
    evalin('base','nvremove'), evalin('base','FIX_dim'), evalin('base','FIX_train'),...
    evalin('base','tempfilt'), evalin('base','filter_kind'), evalin('base','lpcutoff'), evalin('base','hpcutoff'),...
    evalin('base','smoothing'), evalin('base','fwhm_val'));
close(Surface_fMRI)


% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(Surface_fMRI)


% --- Executes on button press in SAVE.
function SAVE_Callback(hObject, eventdata, handles)
% hObject    handle to SAVE (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
startingFolder = pwd;
cd(startingFolder);
folder = uigetdir('./');
handles.ListName = folder;
guidata(hObject, handles);
save_folder = handles.ListName;

save_surface_fMRI(save_folder, evalin('base','wb_folder'), evalin('base','fMRI_total_list'), evalin('base','reorient'), evalin('base','orientation'),...
    evalin('base','delnvol'), evalin('base','VOLorSEC'), evalin('base','VOLorSEC_enterval'),...
    evalin('base','motscrub'), evalin('base','FDthresh_enterval'), evalin('base','motcor'),...
    evalin('base','stcor'), evalin('base','stcor2'), evalin('base','stcor_opt'), evalin('base','slice_order_file'),...
    evalin('base','discor'), evalin('base','reverse_path'), evalin('base','reverse_name'), evalin('base','reverse_ext'),...
    evalin('base','total_readout_time'), evalin('base','skullremoval_fMRI'), evalin('base','intnorm'),...
    evalin('base','regis_fMRI'), evalin('base','T1_total_list'), evalin('base','dof1'),...
    evalin('base','standard_path'), evalin('base','standard_name'), evalin('base','standard_ext'), evalin('base','dof2'),...
    evalin('base','nvremove'), evalin('base','FIX_dim'), evalin('base','FIX_train'),...
    evalin('base','tempfilt'), evalin('base','filter_kind'), evalin('base','lpcutoff'), evalin('base','hpcutoff'),...
    evalin('base','smoothing'), evalin('base','fwhm_val'));
