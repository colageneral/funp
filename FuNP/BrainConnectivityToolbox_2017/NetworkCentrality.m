function [BC_total, DC, EVC] = NetworkCentrality(CorrMat)
%BETWEENNESS_WEI    Node betweenness centrality
%
%   BC = betweenness_wei(G);
%
%   Node betweenness centrality is the fraction of all shortest paths in
%   the network that contain a given node. Nodes with high values of
%   betweenness centrality participate in a large number of shortest paths.
%
%   Input:      G,      connectivity matrix
%                       The connectivity matrix automatically converted to
%                       directed/undirected connection-length matrix.
%                       connection length matrix ~ 1/connectivity matrix
%                       connectivity matrix: raw correlation matrix data
%
%   Output:     BC_total,	1: node betweenness centrality vector.
%                           2: hubness of the node
%                           3: 1, if a given node is a hub node; 0, o/w
%
%   Notes:
%       The input matrix must be a connection-length matrix, typically
%   obtained via a mapping from weight to length. For instance, in a
%   weighted correlation network higher correlations are more naturally
%   interpreted as shorter distances and the input matrix should
%   consequently be some inverse of the connectivity matrix.
%       Betweenness centrality may be normalised to the range [0,1] as
%   BC/[(N-1)(N-2)], where N is the number of nodes in the network.
%
%   Reference: Brandes (2001) J Math Sociol 25:163-177.
%
%
%   Mika Rubinov, UNSW/U Cambridge, 2007-2012
%
%DEGREES_UND        Degree
%
%   deg = degrees_und(G);
%
%   Node degree is the number of links connected to the node.
%
%   Input:      G,    undirected (binary/weighted) connection matrix
%                       weighted connectivity matrix
%
%   Output:     deg,    node degree
%
%   Note: Weight information is discarded.
%
%
%   Olaf Sporns, Indiana University, 2002/2006/2008
%
%EIGENVECTOR_CENTRALITY_UND      Spectral measure of centrality
%
%   v = eigenvector_centrality_und(G)
%
%   Eigenector centrality is a self-referential measure of centrality:
%   nodes have high eigenvector centrality if they connect to other nodes
%   that have high eigenvector centrality. The eigenvector centrality of
%   node i is equivalent to the ith element in the eigenvector
%   corresponding to the largest eigenvalue of the adjacency matrix.
%
%   Inputs:     G,        binary/weighted undirected adjacency matrix.
%                           weighted connectivity matrix
%
%   Outputs:      v,        eigenvector associated with the largest
%                           eigenvalue of the adjacency matrix G.
%
%   Reference: Newman, MEJ (2002). The mathematics of networks.
%
%   Xi-Nian Zuo, Chinese Academy of Sciences, 2010
%   Rick Betzel, Indiana University, 2012

%%%%%%%%%%%%%%%%%%%%%%%%% Betweenness centrality %%%%%%%%%%%%%%%%%%%%%%%%%
G = CorrMat;
E=find(G); G(E)=1./G(E);        %invert weights: Weight conversion
n=length(G);                    %number of nodes
BC=zeros(n,1);                  %vertex betweenness

for u=1:n
    D=inf(1,n); D(u)=0;         %distance from u
    NP=zeros(1,n); NP(u)=1;     %number of paths from u
    S=true(1,n);                %distance permanence (true is temporary)
    P=false(n);                 %predecessors
    Q=zeros(1,n); q=n;          %order of non-increasing distance
    
    G1=G;
    V=u;
    while 1
        S(V)=0;                 %distance u->V is now permanent
        G1(:,V)=0;              %no in-edges as already shortest
        for v=V
            Q(q)=v; q=q-1;
            W=find(G1(v,:));                %neighbours of v
            for w=W
                Duw=D(v)+G1(v,w);           %path length to be tested
                if Duw<D(w)                 %if new u->w shorter than old
                    D(w)=Duw;
                    NP(w)=NP(v);            %NP(u->w) = NP of new path
                    P(w,:)=0;
                    P(w,v)=1;               %v is the only predecessor
                elseif Duw==D(w)            %if new u->w equal to old
                    NP(w)=NP(w)+NP(v);      %NP(u->w) sum of old and new
                    P(w,v)=1;               %v is also a predecessor
                end
            end
        end
        
        minD=min(D(S));
        if isempty(minD), break             %all nodes reached, or
        elseif isinf(minD),                 %...some cannot be reached:
            Q(1:q)=find(isinf(D)); break	%...these are first-in-line
        end
        V=find(D==minD);
    end
    
    DP=zeros(n,1);                          %dependency
    for w=Q(1:n-1)
        BC(w)=BC(w)+DP(w);
        for v=find(P(w,:))
            DP(v)=DP(v)+(1+DP(w)).*NP(v)./NP(w);
        end
    end
end

for i = 1:length(nonzeros(BC~=Inf)),
    BC_norm(i,1) = BC(i,1)/(sum(BC(BC~=Inf))/length(nonzeros(BC~=Inf)));
end;

% BC_total
% 1st column: Original BC value
% 2nd column: Normalized BC value
% 3rd column: 1 - hub node, 0 - not hub node
BC_total(:,1) = BC;
BC_total(:,2) = BC_norm;
for i = 1:length(nonzeros(BC~=Inf))
    if BC_total(i,2) >= 1.5,
        BC_total(i,3) = 1;
    else
        BC_total(i,3) = 0;
    end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%% Degree centrality %%%%%%%%%%%%%%%%%%%%%%%%%
G = CorrMat;
DC = sum(G)';

%%%%%%%%%%%%%%%%%%%%%%%%% Eigenvector centrality %%%%%%%%%%%%%%%%%%%%%%%%%
G = CorrMat;
n = length(G) ;
if n < 1000
    [V,~] = eig(G) ;
    ec = abs(V(:,n)) ;
else
    [V, ~] = eigs(sparse(G)) ;
    ec = abs(V(:,1)) ;
end
EVC = reshape(ec, length(ec), 1);
end