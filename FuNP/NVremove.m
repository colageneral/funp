function varargout = NVremove(varargin)
% NVREMOVE MATLAB code for NVremove.fig
%      NVREMOVE, by itself, creates a new NVREMOVE or raises the existing
%      singleton*.
%
%      H = NVREMOVE returns the handle to a new NVREMOVE or the handle to
%      the existing singleton*.
%
%      NVREMOVE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NVREMOVE.M with the given input arguments.
%
%      NVREMOVE('Property','Value',...) creates a new NVREMOVE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before NVremove_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to NVremove_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help NVremove

% Last Modified by GUIDE v2.5 08-May-2018 15:01:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @NVremove_OpeningFcn, ...
                   'gui_OutputFcn',  @NVremove_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before NVremove is made visible.
function NVremove_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to NVremove (see VARARGIN)

% Choose default command line output for NVremove
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes NVremove wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = NVremove_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in YN.
function YN_Callback(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns YN contents as cell array
%        contents{get(hObject,'Value')} returns selected item from YN
contents = cellstr(get(hObject,'String'));
YN = contents{get(hObject,'Value')};
handles.YN = YN;
guidata(hObject, handles);

if strcmp(handles.YN, 'YES') == 1
    handles.dimension.Enable='on';
    handles.FixTrain.Enable='on';
else 
    handles.dimension.Enable='off';
    handles.FixTrain.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function YN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in dimension.
function dimension_Callback(hObject, eventdata, handles)
% hObject    handle to dimension (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns dimension contents as cell array
%        contents{get(hObject,'Value')} returns selected item from dimension
contents = cellstr(get(hObject,'String'));
dim_opt = contents{get(hObject,'Value')};
handles.dim_opt = dim_opt;
guidata(hObject, handles);

if strcmp(handles.dim_opt, 'Auto') == 1
    handles.ManualDim.Enable='off';
elseif strcmp(handles.dim_opt, 'Manual') == 1
    handles.ManualDim.Enable='on';
else 
    handles.ManualDim.Enable='off';
end


% --- Executes during object creation, after setting all properties.
function dimension_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dimension (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ManualDim_Callback(hObject, eventdata, handles)
% hObject    handle to ManualDim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ManualDim as text
%        str2double(get(hObject,'String')) returns contents of ManualDim as a double
manual_dim = str2double(get(hObject,'String'));
handles.manual_dim = manual_dim;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function ManualDim_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ManualDim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on selection change in FixTrain.
function FixTrain_Callback(hObject, eventdata, handles)
% hObject    handle to FixTrain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns FixTrain contents as cell array
%        contents{get(hObject,'Value')} returns selected item from FixTrain
contents = cellstr(get(hObject,'String'));
training_file = contents{get(hObject,'Value')};
handles.training_file = training_file;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function FixTrain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FixTrain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ischar(handles.YN) == 1
    nvremove = handles.YN;
    assignin('base', 'nvremove', nvremove);

    if strcmp(handles.YN, 'YES') == 1
        
        fn = fieldnames(handles);
        test1 = 0;
        for i = 1:size(fn,1)
            if strcmp(fn{i}, 'dim_opt') == 1
                test1 = test1 + 1;
            end
        end
        if test1 == 0
            error('### You did not select dimension option ###')
        else
            if strcmp(handles.dim_opt, 'Auto') == 1
                Fix_dim = 0;
                assignin('base', 'FIX_dim', Fix_dim);
            else
                
                test3 = 0;
                for i = 1:size(fn,1)
                    if strcmp(fn{i}, 'manual_dim') == 1
                        test3 = test3 + 1;
                    end
                end
                
                if test3 == 1
                    if isnan(handles.manual_dim) ~= 1
                        assignin('base', 'FIX_dim', handles.manual_dim);
                    else
                        error('### You entered non-numeric value ###')
                    end                    
                else
                    error('### You did not enter manual dimension ###')
                end
            end
        end
        
        test2 = 0;
        for i = 1:size(fn,1)
            if strcmp(fn{i}, 'training_file') == 1
                test2 = test2 + 1;
            end
        end
        if test2 == 0
            error('### You did not select training file ###')
        else
            assignin('base', 'FIX_train', handles.training_file);
        end
        
    else
        FIX_dim = [];
        FIX_train = [];
        assignin('base', 'FIX_dim', FIX_dim);
        assignin('base', 'FIX_train', FIX_train);
    end

else
    error('### You did not select Y/N option ###')
end
if length(nvremove) == 0
    error('### You did not select Y/N option ###')
end

close(NVremove)



% --- Executes on button press in CANCEL.
function CANCEL_Callback(hObject, eventdata, handles)
% hObject    handle to CANCEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(NVremove)
